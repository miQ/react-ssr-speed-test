import { all } from 'redux-saga/effects';
import usersSaga from './users/users.sagas';
import postsSaga from './posts/posts.sagas';

export default function* rootSaga() {
  yield all([
    usersSaga(),
    postsSaga(),
  ]);
}
