import { call, put, takeLatest, all } from 'redux-saga/effects';

import { get } from '../api/api.sagas';
import { PostsTypes, PostsActions } from './posts.redux';

const PAGE_SIZE = 5;
const CHUNKS = 5;

const fetchPost = (seed, id) => call(get, `https://jsonplaceholder.typicode.com/posts/${parseInt(seed, 10) + id}`);

export function* fetchPostsSaga(opts) {
  const { seed } = opts;
  try {
    const srcArr = Array.from(Array(PAGE_SIZE));

    let data = [];
    for (let i = 0; i < CHUNKS; i++) {
      const chunk = yield all(srcArr.map((val, id) => fetchPost(seed, id + i * PAGE_SIZE)));
      data = [...data, ...chunk];
    }

    yield put(PostsActions.fetchSuccess(data));
  } catch (e) {
    yield put(PostsActions.fetchError(e));
  }
}

export default function* postsSaga() {
  yield all([
    takeLatest(PostsTypes.FETCH, fetchPostsSaga),
  ]);
}
