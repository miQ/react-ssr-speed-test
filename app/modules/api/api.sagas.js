import { call } from 'redux-saga/effects';
import { assign, memoize } from 'lodash';
import { stringify } from 'query-string';

let _lastCache = null;
const CACHE_DURATION = 10000;

export function parseJSON(response) {
  return response.json();
}

const memoizedFetch = memoize((url, options) => {
  console.log('I\'m in cachedFetch() for ' + url);
  return call(fetch, url, assign({ }, options));
});

const fetchCached = (url, options) => {
  const now = new Date();
  if (_lastCache && (now - _lastCache > CACHE_DURATION)) {
    memoizedFetch.cache.clear();
  }
  _lastCache = new Date();
  return memoizedFetch(url, options);
};

export function* requestSaga(url, options) {
  try {
    const response = yield fetchCached(url, options);
    return yield call(parseJSON, response);
  } catch (e) {
    return yield call(parseJSON, e);
  }
}

export function* get(url, body = {}, options = {}) {
  return yield call(requestSaga, `${url}?${stringify(body)}`, assign({ method: 'GET' }, options));
}

export function* post(url, body = {}, options = {}) {
  return yield call(requestSaga, url, assign({ method: 'POST', body }, options));
}
