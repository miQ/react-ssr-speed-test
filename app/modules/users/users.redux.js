import { createActions, createReducer } from 'reduxsauce';
import { Record, List, fromJS } from 'immutable';

export const { Types: UsersTypes, Creators: UsersActions } = createActions({
  fetch: ['seed'],
  fetchSuccess: ['data'],
  fetchError: ['payload'],
}, { prefix: 'USERS_' });

const UsersRecord = new Record({
  items: List(),
});

export const INITIAL_STATE = new UsersRecord({});

const getSuccessHandler = (state = INITIAL_STATE, action) => state.set('items', fromJS(action.data));

export const reducer = createReducer(INITIAL_STATE, {
  [UsersTypes.FETCH_SUCCESS]: getSuccessHandler,
});
