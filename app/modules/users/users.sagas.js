import { call, put, takeLatest, all } from 'redux-saga/effects';

import { get } from '../api/api.sagas';
import { UsersTypes, UsersActions } from './users.redux';

const PAGE_SIZE = 2;

const fetchUser = (seed, id) => call(get, 'https://randomuser.me/api', { results: 1, seed, page: id + 1 });

export function* fetchUsersSaga({ seed }) {
  try {
    const response = yield all(Array.from(Array(PAGE_SIZE)).map((val, id) => fetchUser(seed, id)));
    const data = response.map(r => r.results[0]);

    yield put(UsersActions.fetchSuccess(data));
  } catch (e) {
    yield put(UsersActions.fetchError(e));
  }
}

export default function* usersSaga() {
  yield all([
    takeLatest(UsersTypes.FETCH, fetchUsersSaga),
  ]);
}
