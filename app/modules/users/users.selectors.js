import { createSelector } from 'reselect';


const selectUsersDomain = state => state.get('users');

export const selectUsersItems = createSelector(
  selectUsersDomain, state => state.get('items')
);
