import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Home } from './home.component';
import { UsersActions } from '../../modules/users/users.redux';
import { PostsActions } from '../../modules/posts/posts.redux';
import { selectUsersItems } from '../../modules/users/users.selectors';
import { selectPostsItems } from '../../modules/posts/posts.selectors';
import { LocalesActions } from '../../modules/locales/locales.redux';
import { selectLocalesLanguage } from '../../modules/locales/locales.selectors';

const mapStateToProps = createStructuredSelector({
  users: selectUsersItems,
  posts: selectPostsItems,
  language: selectLocalesLanguage,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchUsers: UsersActions.fetch,
  fetchPosts: PostsActions.fetch,
  setLanguage: LocalesActions.setLanguage,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
