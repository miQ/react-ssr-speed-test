import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';

export class Home extends PureComponent {
  static propTypes = {
    users: PropTypes.object,
    posts: PropTypes.object,
    language: PropTypes.string.isRequired,
    fetchUsers: PropTypes.func.isRequired,
    fetchPosts: PropTypes.func.isRequired,
    router: PropTypes.any,
  };

  constructor(props) {
    super(props);

    const { users } = this.props;
    if (!users || users.size === 0) {
      const { seed } = this.props.router.params;
      this.props.fetchUsers(seed);
      this.props.fetchPosts(seed);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.router.params.seed !== this.props.router.params.seed) {
      const { seed } = this.props.router.params;
      this.props.fetchUsers(seed);
      this.props.fetchPosts(seed);
    }
  }

  render() {
    return (
      <div className="home">
        <Helmet title="Homepage" />

        <h2>USERS</h2>
        <ul className="list">
          {this.props.users.map((u, id) => (
            <li key={id}>
              <span>{u.getIn(['name', 'title'])}</span>
              <span>{u.getIn(['name', 'first'])}</span>
              <span>{u.getIn(['name', 'last'])}</span>
            </li>
          ))}
        </ul>

        <h2>POSTS</h2>
        <ul className="list">
          {this.props.posts.map((u, id) => (
            <li key={id}>
              <span>[{u.get('id')}]</span>
              <span>{u.get('title')}</span>
            </li>
          ))}
        </ul>

      </div>
    );
  }
}
